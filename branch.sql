CREATE TABLE stores(
    id INT NOT NULL, 
    store_name TEXT NOT NULL, 
    PRIMARY KEY(id)
);

INSERT INTO stores VALUES
    (1,'Stanely Book Store'),
    (2,'Vijaya Book Store'),
    (3,'Natraj Book Store');

CREATE TABLE branch(
    id INT NOT NULL, 
    branch_name TEXT NOT NULL, 
    branch_address TEXT, 
    PRIMARY KEY(id)
);

INSERT INTO branch VALUES
    (1,'bangalore','madiwala'),
    (2,'chennai','thambaram');

CREATE TABLE stores_and_branches(
    id INT NOT NULL PRIMARY KEY, 
    store_id INT NOT NULL, 
    branch_id INT NOT NULL, 
    FOREIGN KEY(store_id) REFERENCES stores(id), 
    FOREIGN KEY(branch_id) REFERENCES branch(id), 
    UNIQUE(store_id,branch_id)
);

INSERT INTO stores_and_branches VALUES
    (1,1,1),
    (2,1,2),
    (3,2,1),
    (4,2,2);

CREATE TABLE books(
    isbn INT NOT NULL PRIMARY KEY, 
    title TEXT NOT NULL, 
    publisher TEXT NOT NULL
);

INSERT INTO books VALUES
    (10001000,'Cutting for Stone','Informa'),
    (10001001,"Benath the lion's Gaze",'Read Elsevier'),
    (10001002,'Words of wisdom','Raders Digest');

CREATE TABLE author(
    id INT NOT NULL PRIMARY KEY,  
    firstname TEXT NOT NULL, 
    lastname TEXT NOT NULL
);

INSERT INTO author VALUES
    (1,'Abraham', 'Verghese'),
    (2,'Maaza', 'Mengister'),
    (3,'Pilaztiko','Zingkiya'),
    (4,'Canytizare','Kundolise');

CREATE TABLE books_author(
    id INT NOT NULL PRIMARY KEY,
    book_id INT NOT NULL,
    author_id INT NOT NULL,
    FOREIGN KEY(book_id) REFERENCES books(isbn),
    FOREIGN KEY(author_id) REFERENCES author(id),
    UNIQUE(book_id,author_id)
);
INSERT INTO books_author VALUES
    (1,10001002,4),
    (2,10001002,3),
    (3,10001001,2),
    (4,10001000,1);
    

CREATE TABLE books_based_on_stores_and_branches(
    id INT NOT NULL PRIMARY KEY, 
    store_and_branch_id INT NOT NULL, 
    books_id INT NOT NULL,
    num_copies INT,
    FOREIGN KEY(store_and_branch_id) REFERENCES stores_and_branches(id), 
    FOREIGN KEY(books_id) REFERENCES books(isbn),
    UNIQUE(books_id,store_and_branch_id)
);

INSERT INTO books_based_on_stores_and_branches VALUES
    (1,1,10001000,300),
    (2,1,10001001,300),
    (3,2,10001000,100),
    (4,2,10001001,200),
    (5,1,10001002,330),
    (6,2,10001002,500),
    (7,4,10001001,100),
    (8,3,10001000,60);