CREATE TABLE doctor(
    id INT NOT NULL PRIMARY KEY, 
    Doctor_name TEXT NOT NULL, 
    Secretary TEXT NOT NULL
);

INSERT INTO doctor VALUES
    (1,'Karthikeyan','Pavi'),
    (2,'Sugavaneswari','darshana');

CREATE TABLE patient(
    id INT NOT NULL PRIMARY KEY, 
    Name TEXT NOT NULL, 
    DOB DATE, 
    Address TEXT NOT NULL, 
    City TEXT NOT NULL
);

INSERT INTO patient VALUES
    (1,'kowsi','1997-12-01','23 roja salai','salem'),
    (2,'Sowmiya','1993-08-04','Rajaji Nagar','Trichy');

CREATE TABLE appointments(
    id INT NOT NULL PRIMARY KEY,
    appointment DATE NOT NULL UNIQUE
);

INSERT INTO appointments VALUES
    (1,'2020-07-23'),
    (2,'2020-04-24');
    
CREATE TABLE patient_appointments(
    id INT NOT NULL PRIMARY KEY,
    patient_id INT NOT NULL,
    appointment_id INT NOT NULL,
    FOREIGN KEY(patient_id) REFERENCES patient(id),
    FOREIGN KEY(appointment_id) REFERENCES appointments(id),
    UNIQUE(patient_id,appointment_id)
);

INSERT INTO patient_appointments VALUES 
    (1,1,1),
    (2,1,2),
    (3,2,2);


CREATE TABLE medication(
    medicine_id INT NOT NULL PRIMARY KEY, 
    Drug TEXT NOT NULL, 
    Dosage TEXT NOT NULL
);

INSERT INTO medication VALUES
    (1,'Aspirin','once a day'),
    (2,'Benzonatate','twice a day'),
    (3,'Toprol XL','once a week');

CREATE TABLE doctor_patient_appointment(
    id INT NOT NULL PRIMARY KEY, 
    patient_appointment_id INT NOT NULL, 
    doctor_id INT NOT NULL, 
    FOREIGN KEY(patient_appointment_id) REFERENCES patient_appointments(id), 
    FOREIGN KEY(doctor_id) REFERENCES doctor(id),
    UNIQUE(patient_appointment_id, doctor_id)
);

INSERT INTO doctor_patient_appointment VALUES
    (1,1,1),
    (2,1,2),
    (3,2,1),
    (4,2,2);

CREATE TABLE doctor_patient_medication(
    id INT NOT NULL PRIMARY KEY, 
    prescribes_id INT NOT NULL,
    doctor_patient_id INT NOT NULL, 
    FOREIGN KEY(prescribes_id) REFERENCES medication(medicine_id),
    FOREIGN KEY(doctor_patient_id) REFERENCES doctor_patient_appointment(id),
    UNIQUE(doctor_patient_id,prescribes_id)
);

INSERT INTO doctor_patient_medication VALUES
    (1,1,1),
    (2,1,3),
    (3,2,1),
    (4,3,2),
    (5,3,3);