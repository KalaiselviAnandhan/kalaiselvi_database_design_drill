CREATE TABLE city(
    id INT NOT NULL PRIMARY KEY,
    city_name TEXT NOT NULL
);

INSERT INTO city VALUES
    (1,'bangalore'),
    (2,'chennai'),
    (3,'salem'),
    (4,'vellore'),
    (5,'namakkal');

CREATE TABLE state(
    id INT NOT NULL PRIMARY KEY,
    state_name TEXT NOT NULL
);

INSERT INTO state VALUES
    (1,'karnataka'),
    (2,'tamil nadu');
    
CREATE TABLE location(
    id INT NOT NULL PRIMARY KEY,
    city_id INT NOT NULL,
    state_id INT NOT NULL,
    FOREIGN KEY(city_id) REFERENCES city(id),
    FOREIGN KEY(state_id) REFERENCES state(id),
    UNIQUE(city_id, state_id)
);

INSERT INTO location VALUES
    (1,1,1),
    (2,2,2),
    (3,3,2),
    (4,4,2),
    (5,5,2);

CREATE TABLE client(
    id INT NOT NULL PRIMARY KEY, 
    client_name TEXT NOT NULL, 
    client_location_id INT NOT NULL, 
    FOREIGN KEY(client_location_id) REFERENCES location(id)
);

INSERT INTO client VALUES
    (1,'Finestra',1),
    (2,'Minomi',2);

CREATE TABLE manager(
    id INT NOT NULL PRIMARY KEY, 
    firstname TEXT NOT NULL,
    lastname TEXT NOT NULL,
    manager_location_id INT NOT NULL, FOREIGN KEY(manager_location_id) REFERENCES location(id)
);

INSERT INTO manager VALUES
    (1,'Ranjan','Kesari',1),
    (2,'Manjunath','Rajath',1),
    (3,'Priya','Anand',1);

CREATE TABLE staff(
    id INT NOT NULL PRIMARY KEY, 
    firstname TEXT NOT NULL,
    lastname TEXT NOT NULL,
    staff_location_id INT NOT NULL, FOREIGN KEY(staff_location_id) REFERENCES location(id)
);

INSERT INTO staff VALUES
    (1,'priya','shan',2), 
    (2,'darshana','govind',5), 
    (3,'preethika','gunasekaran',3), 
    (4,'maha','pandian',4), 
    (5,'monika','jonraj',1),
    (6,'sureka','sivakumar',1);

CREATE TABLE contract_info(
    id INT NOT NULL PRIMARY KEY, 
    estimated_cost BIGINT NOT NULL, 
    completion_date DATE NOT NULL
);

INSERT INTO contract_info VALUES
    (1,12311100,'2022-09-12'),
    (2,11411100,'2023-08-06');

CREATE TABLE client_manager(
    id INT NOT NULL PRIMARY KEY, 
    client_id INT, 
    manager_id INT NOT NULL,
    FOREIGN KEY(client_id) REFERENCES client(id),
    FOREIGN KEY(manager_id) REFERENCES manager(id),
    UNIQUE(client_id,manager_id)
);

INSERT INTO client_manager VALUES
    (1,1,1),
    (2,1,2),
    (3,2,3);


CREATE TABLE client_manager_staff(
    id INT NOT NULL PRIMARY KEY, 
    staff_id INT NOT NULL UNIQUE, 
    client_manager_id INT, 
    contract_id INT NOT NULL,
    FOREIGN KEY(staff_id) REFERENCES staff(id),
    FOREIGN KEY(client_manager_id) REFERENCES client_manager(id),
    FOREIGN KEY(contract_id) REFERENCES contract_info(id)
);
INSERT INTO client_manager_staff VALUES
    (1,1,1,1),
    (2,2,1,1),
    (3,3,2,2),
    (4,4,2,2),
    (5,5,3,1),
    (6,6,3,1);